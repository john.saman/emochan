from gensim.models import word2vec
from emoji.unicode_codes import EMOJI_UNICODE

model = word2vec.Word2Vec.load("./emochan.model")

emojis = []
for emoji in EMOJI_UNICODE.keys():
    if emoji in model.wv:
        emojis.append(emoji)

most_similar = model.wv.most_similar_to_given('嬉しい', emojis)
print(EMOJI_UNICODE[most_similar])


## IDEA: get most_similar word from vector representing a sentence, then use most_similar_to_given to get the nearest emojis
