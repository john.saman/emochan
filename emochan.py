from flask import Flask, request, abort

from linebot import (
    LineBotApi, WebhookHandler
)
from linebot.exceptions import (
    InvalidSignatureError
)
from linebot.models import (
    MessageEvent, TextMessage, TextSendMessage,
)
import os

from gensim.models import word2vec
from emoji.unicode_codes import EMOJI_UNICODE

app = Flask(__name__)

#LINE Access Token
YOUR_CHANNEL_ACCESS_TOKEN = "hObXQhLcmF7S6aTBZ5v6UFzV+1gaKX/1BFh9lhUkViUjfrzhjL3wXz/8434VgwTwIqQxiUZrU9dHx3Z2kzCV11yjLG5F4ZQjeNTZqZ1dk6/gi+QLL2ecznFLbSFispUgcPrFrPiF0TiKT5u/v7crwwdB04t89/1O/w1cDnyilFU="
#LINE Channel Secret
YOUR_CHANNEL_SECRET = "5cad4767f3baf1c2455617ddec21076e"

line_bot_api = LineBotApi(YOUR_CHANNEL_ACCESS_TOKEN)
handler = WebhookHandler(YOUR_CHANNEL_SECRET)

model = word2vec.Word2Vec.load("./emochan.model")

emojis = []
for emoji in EMOJI_UNICODE.keys():
    if emoji in model.wv:
        emojis.append(emoji)

@app.route("/callback", methods=['POST'])
def callback():
    # get X-Line-Signature header value
    signature = request.headers['X-Line-Signature']

    # get request body as text
    body = request.get_data(as_text=True)
    app.logger.info("Request body: " + body)

    # handle webhook body
    try:
        handler.handle(body, signature)
    except InvalidSignatureError:
        abort(400)

    return 'OK'


@handler.add(MessageEvent, message=TextMessage)
def handle_message(event):
    #vector = model.wv[event.message.text]
    most_similar = model.wv.most_similar_to_given(event.message.text, emojis)
    print(EMOJI_UNICODE[most_similar])
    line_bot_api.reply_message(
        event.reply_token,
        TextSendMessage(text=EMOJI_UNICODE[most_similar]))


if __name__ == "__main__":
#    app.run()
    port = int(os.getenv("PORT"))
    app.run(host="0.0.0.0", port=port)
