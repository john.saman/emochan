
# importing the module
import re
import esanpy
from emoji.unicode_codes import UNICODE_EMOJI

esanpy.start_server()

def tokenize(tweet_text):
    partial_tweet_text = ""
    tokens = []
    for s in tweet_text:
        if s in UNICODE_EMOJI.keys():
            tokens.extend([UNICODE_EMOJI[s]])
            tokens.extend(esanpy.analyzer(partial_tweet_text, analyzer="kuromoji"))
            partial_tweet_text = ""
        else:
            partial_tweet_text += s

    return tokens

f = open("corpus.txt", "r")
for tweet in f:
    tweet = re.sub(r"http\S+", "", tweet)
    tweet = re.sub(r"@\S+", "", tweet)

    tokens = tokenize(tweet)

    if len(tokens) > 0:
        print(" ".join(tokens))
