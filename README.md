# Description
* Emochan is sentence to emoji converter chat bot.

# Install required libraries
```shell
  pip install -r requirements.txt
```

# Generate corpus
```shell
  python3 generate_corpus.py >> corpus.txt
```

# Tokenize corpus
```shell
  python3 tokenize_corpus.py > tokenized_corpus.txt
```

# Create word2vec model
```shell
  python3 create_model.py
```

# Configuration for nginx (require domain & ssl certificate)
```nginx
server {
    listen              443 ssl;
    server_name         www.johnsaman.ml;
    ssl_certificate     /path/to/fullchain.pem;
    ssl_certificate_key /path/to/privkey.pem;
    ssl_protocols       TLSv1 TLSv1.1 TLSv1.2;
    ssl_ciphers         HIGH:!aNULL:!MD5;

    location / {
        proxy_pass http://localhost:8000;
    }
}
```
# Run emochan chatbot
```shell
  PORT=8000 python3 emochan.py
```
