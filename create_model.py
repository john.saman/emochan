from gensim.models import word2vec
sentences = word2vec.Text8Corpus('./tokenized_corpus.txt')

model = word2vec.Word2Vec(sentences, size=200, min_count=20, window=15)
model.save("./emochan.model")
